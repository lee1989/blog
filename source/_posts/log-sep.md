---
title: 日志分表
date: 2016年4月6日 10:56:43
tags:
- 唯中科技
categories:
- 工作日志
---
逻辑梳理：
<!--more-->

##  直接运行下面的php代码便可得到所有表的分表sql语句：
```php
<?php
function gen_partion_log($tables, $db_name) {
    $sql = '';
    foreach ($tables as $tb) {
        $sql .= <<<EOF
-- 建立临时分区表：
-- 拷贝表结构：
Delimiter ;
drop table if exists {$tb['name']}_tmp;
create table {$tb['name']}_tmp like {$tb['name']};
-- RANGE COLUMNS分区要求分区列不能是varchar类型，所以，要将add_time改为int类型：
ALTER TABLE `{$tb['name']}_tmp` CHANGE COLUMN `{$tb['timefield']}` `{$tb['timefield']}` INT NOT NULL COMMENT '添加时间-时间戳';
-- 分区列必须包含在主键里，所以要将{$tb['timefield']}包含进主键：
ALTER TABLE `{$tb['name']}_tmp`    DROP PRIMARY KEY,  ADD PRIMARY KEY (`id`, `{$tb['timefield']}`);
-- 增加分区（按月分区）：
ALTER TABLE `{$tb['name']}_tmp`  PARTITION BY RANGE COLUMNS({$tb['timefield']})
(PARTITION p201511 VALUES LESS THAN (UNIX_TIMESTAMP('2015-12-01')),
 PARTITION p201512 VALUES LESS THAN (UNIX_TIMESTAMP('2016-01-01')),
 PARTITION p201601 VALUES LESS THAN (UNIX_TIMESTAMP('2016-02-01')),
 PARTITION p201602 VALUES LESS THAN (UNIX_TIMESTAMP('2016-03-01')),
 PARTITION p201603 VALUES LESS THAN (UNIX_TIMESTAMP('2016-04-01')),
 PARTITION p201604 VALUES LESS THAN (UNIX_TIMESTAMP('2016-05-01')),
 PARTITION p201605 VALUES LESS THAN (UNIX_TIMESTAMP('2016-06-01'))
 );

-- 从{$tb['name']}表中导入数据：
insert into {$tb['name']}_tmp select * from {$tb['name']};
-- 将临时表和正式表互换：
drop table if exists {$tb['name']}_older;
rename table {$tb['name']} to {$tb['name']}_older;
rename table {$tb['name']}_tmp to {$tb['name']};

-- 创建数据库定时任务（每个月的1号的凌晨1点执行上面创建的存储器）：
DELIMITER $$
DROP EVENT IF EXISTS `eSetPartition_{$tb['name']}`;
$$
CREATE EVENT eSetPartition_{$tb['name']}
ON SCHEDULE EVERY 1 MONTH STARTS DATE_ADD(DATE_ADD(DATE_SUB(CURDATE(),INTERVAL DAY(CURDATE())-1 DAY), INTERVAL 1 MONTH),INTERVAL 1 HOUR)   
ON COMPLETION PRESERVE ENABLE  
COMMENT '对{$tb['name']}表每个月1号的凌晨1点自动执行分区' 
DO
BEGIN
    call SetPartitionTimestamp('{$tb['name']}');
END;
$$



EOF;
    }
    $sql .= <<<EOF
-- 创建存储过程来自动为每个月创建分区：
DELIMITER $$
DROP PROCEDURE IF EXISTS `SetPartitionTimestamp`;
$$
CREATE PROCEDURE SetPartitionTimestamp(IN tb_name varchar(30))
COMMENT '对timestamp字段类型按每个月划分分区'
BEGIN
    SET @db_name = '{$db_name}';
    SET @part_prefix = 'p';
    SELECT REPLACE(partition_name,@part_prefix,'') INTO @last_year_mon
        FROM INFORMATION_SCHEMA.PARTITIONS
        WHERE TABLE_SCHEMA=@db_name AND table_name=tb_name
        ORDER BY partition_ordinal_position DESC
        LIMIT 1;
    select PERIOD_ADD(@last_year_mon,1) into @next_year_mon;
    select PERIOD_ADD(@next_year_mon,1) into @next_next_year_mon;
    SET @exec_sql = CONCAT('ALTER TABLE `', tb_name, '` ADD PARTITION (PARTITION ', @part_prefix, @next_year_mon, ' VALUES LESS THAN (UNIX_TIMESTAMP(\'', @next_next_year_mon, '01\')))');
    PREPARE stmt2 FROM @exec_sql;
    EXECUTE stmt2;
    DEALLOCATE PREPARE stmt2;
END;
$$
-- 开启event：
SET GLOBAL event_scheduler = ON;
SET @@global.event_scheduler = ON;
SET GLOBAL event_scheduler = 1;
SET @@global.event_scheduler = 1;
$$
EOF;
    return $sql;
}

$tables = [ // 表名、分列字段
    ['name' => 'dx_zhushou_update_log'   , 'timefield' => 'updateDate'],
    ['name' => 'dx_zhushou_uninstall_log', 'timefield' => 'updateDate'],
    ['name' => 'dx_zhushou_run_log'      , 'timefield' => 'updateDate'],
    ['name' => 'dx_zhushou_install_log'  , 'timefield' => 'updateDate'],
    ['name' => 'dx_zhushou_down_log'     , 'timefield' => 'downDate'  ],
    ['name' => 'dx_syb_down_log'         , 'timefield' => 'downDate'  ],
    ['name' => 'dx_simulator_run_log'    , 'timefield' => 'updateDate'],
    ['name' => 'dx_simulator_install_log', 'timefield' => 'updateDate'],
    ['name' => 'dx_phone_install_log'    , 'timefield' => 'updateDate'],
    ['name' => 'dx_game_log'             , 'timefield' => 'add_time'  ],
];

echo gen_partion_log($tables, 'scp_shuowan_tj_local')."\n";
```

## 运行下面的sql语句可用于查询分表后的性能
```sql
--查看插入的数据是否有按分区存放：
Delimiter ;
SELECT PARTITION_NAME as 分区名,PARTITION_ORDINAL_POSITION as 分区顺序, PARTITION_DESCRIPTION as 分区条件, TABLE_ROWS as 分区行数
FROM information_schema.PARTITIONS 
WHERE TABLE_SCHEMA='scp_shuowan_tj_local' AND TABLE_NAME = 'dx_game_log';
--对比分区前后查询速度：
--分区前的查询速度（结果：查询时间约0.4s）：
select count(*) from dx_game_log_older where add_time >= unix_timestamp('2016-03-01') and add_time < unix_timestamp('2016-04-01');
--分区后的查询速度（结果：查询时间约0.1s）：
select count(*) from dx_game_log where add_time >= unix_timestamp('2016-03-01') and add_time < unix_timestamp('2016-04-01');
--查看分区后，执行的查询有扫描哪些分区（结果：只扫描了p201603分区。若将<改为<=，则会扫描p201603,p201604两个分区）：
explain partitions select count(*) from dx_game_log where add_time >= unix_timestamp('2016-03-01') and add_time < unix_timestamp('2016-04-01');
```

## 确认无误后，删除旧的未分表的表：
```sql
Delimiter ;
drop table if exists dx_zhushou_update_log_older;
drop table if exists dx_zhushou_uninstall_log_older;
drop table if exists dx_zhushou_run_log_older;
drop table if exists dx_zhushou_install_log_older;
drop table if exists dx_zhushou_down_log_older;
drop table if exists dx_syb_down_log_older;
drop table if exists dx_simulator_run_log_older;
drop table if exists dx_simulator_install_log_older;
drop table if exists dx_phone_install_log_older;
drop table if exists dx_game_log_older;
```
---
